package at.fhs.nos.demojunit4.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	BasicTrueTest.class,
	BasicFalseTest.class,
	ContainerTest.class,
	SingletonTest.class
})
public class SimpleTesteSuit {

}