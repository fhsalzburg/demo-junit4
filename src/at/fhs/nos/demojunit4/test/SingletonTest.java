package at.fhs.nos.demojunit4.test;

import at.fhs.nos.demojunit4.ThreadSafeSingleton;
import org.junit.Test;
import static org.junit.Assert.*;

public class SingletonTest {
	public ThreadSafeSingleton s1 = null;
	public ThreadSafeSingleton s2 = null;
	
	@Test
	public void checkForInstance() {
		Runnable runner1 = new Runnable() {
			@Override
			public void run() {
				s1 = ThreadSafeSingleton.getInstance();
			}			
		};
		Runnable runner2 = new Runnable() {
			@Override
			public void run() {
				s2 = ThreadSafeSingleton.getInstance();
			}			
		};
		
		Thread t1 = new Thread(runner1);
		Thread t2 = new Thread(runner2);
		
		t1.start();
		t2.start();
		
		try {
			t2.join();
			t1.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			assertTrue(false);
		}
		
		assertSame(s1,s2);
	}
}
