package at.fhs.nos.demojunit4.test;

import static org.junit.Assert.*;

import org.junit.Test;
import at.fhs.nos.demojunit4.SimpleContainer;


public class ContainerTest {
	@Test
	public void containerTest() {
		SimpleContainer container = new SimpleContainer();
		
		container.put(400, "hugo");
		
		assertEquals(container.getValueId("hugo"), new Integer(400));
	}
}