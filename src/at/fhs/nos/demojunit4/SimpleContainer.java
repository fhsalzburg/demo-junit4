package at.fhs.nos.demojunit4;

import java.util.Map;
import java.util.HashMap;

public class SimpleContainer {
	Map<Integer, String> simpleContainer = new HashMap<Integer, String>();
	
	public void put(Integer key, String value) {
		simpleContainer.put(key, value);
	}
	
	public Integer getValueId(String text) {
		for (Integer key: simpleContainer.keySet()) {
			if (simpleContainer.get(key).equals(text)) {
				return key;
			}
		}
		
		return null;
	}
}